#include "stdafx.h"
#include "Project.h"

namespace basecross
{
	//----------------------------------------------------------------------------
	//class LateralObject : public GameObject
	//左右に移動する石像（青色）
	//----------------------------------------------------------------------------

	//構築と破棄
	LateralObject::LateralObject(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}

	LateralObject::~LateralObject() {}

	//初期化

	void LateralObject::OnCreate()
	{
		auto PtrTransform = AddComponent<Transform>();


		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		auto PtrObb = AddComponent<CollisionObb>();
		PtrObb->SetFixed(true);
		auto PtrDraw = AddComponent<PNTStaticDraw>();

		//自分の色を白にする
		PtrDraw->SetDiffuse(Color4(0.0, 0.0, 1.0, 1.0));

		//自分の色を見る
		auto MyColor = PtrDraw->GetDiffuse();

		//自分の色が青（Color4([R]0.0,[G]0.0,[B]1.0,[A]1.0)）になっているか見る
		if (MyColor == Color4(0.0, 0.0, 1.0, 1.0))
		{
			Timer += 1.0;
			if (Timer <= 300) {
				//アクションの登録
				auto PtrAction = AddComponent<Action>();
				PtrAction->AddMoveBy(5.0f, Vector3(5.0f, 0.0f, 0));
				PtrAction->AddMoveBy(5.0f, Vector3(-5.0f, 0.0f, 0));

				//ループする
				PtrAction->SetLooped(true);
				//アクション開始
				PtrAction->Run();
			}
		}
		if (Timer >= 400)
		{
			PtrDraw->SetDiffuse(Color4(1.0, 1.0, 1.0, 1.0));
		}

		//影を付ける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_SQUARE");

		PtrDraw->SetMeshResource(L"DEFAULT_SQUARE");

		PtrDraw->SetOwnShadowActive(true);
		PtrDraw->SetTextureResource(L"PICTDASH_TX");

		auto Group = GetStage()->GetSharedObjectGroup(L"LateralObject");
		Group->IntoGroup(GetThis<GameObject>());

		//透明処理
		SetAlphaActive(true);

	}

	//----------------------------------------------------------------------------
	//class JumpObject : public GameObject
	//ジャンプする石像（緑色）
	//----------------------------------------------------------------------------

	//構築と破棄
	JumpObject::JumpObject(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}

	JumpObject::~JumpObject() {}

	//初期化

	void JumpObject::OnCreate()
	{
		auto PtrTransform = AddComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		auto PtrObb = AddComponent<CollisionObb>();
		PtrObb->SetFixed(true);
		auto PtrDraw = AddComponent<PNTStaticDraw>();

		//自分の色を白にする
		PtrDraw->SetDiffuse(Color4(0.0, 1.0, 0.0, 1.0));

		//自分の色を見る
		auto MyColor = PtrDraw->GetDiffuse();

		//自分の色が緑（Color4([R]0.0,[G]1.0,[B]0.0,[A]1.0)）になっているか見る
		if (MyColor == Color4(0.0, 1.0, 0.0, 1.0))
		{
			//アクションの登録
			auto PtrAction = AddComponent<Action>();
			PtrAction->AddMoveBy(5.0f, Vector3(0.0f, 1.0f, 0));
			PtrAction->AddMoveBy(5.0f, Vector3(0.0f, -1.0f, 0));
			//ループする
			PtrAction->SetLooped(true);
			//アクション開始
			PtrAction->Run();
		}
		//影を付ける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_SQUARE");

		PtrDraw->SetMeshResource(L"DEFAULT_SQUARE");

		PtrDraw->SetOwnShadowActive(true);
		PtrDraw->SetTextureResource(L"PICTJUMP_TX");

		auto Group = GetStage()->GetSharedObjectGroup(L"JumpObject");
		Group->IntoGroup(GetThis<GameObject>());

		//透明処理
		SetAlphaActive(true);

	}

	//-------------------------------------------------
	//	class AttackObject : public GameObject
	//壁を破壊する石像
	//-------------------------------------------------

	AttackObject::AttackObject(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}

	AttackObject::~AttackObject() {}

	//初期化

	void AttackObject::OnCreate()
	{
		auto PtrTransform = AddComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		auto PtrObb = AddComponent<CollisionObb>();
		PtrObb->SetFixed(true);

		PtrObb->SetIsHitAction(IsHitAction::AutoOnObjectRepel);
		auto PtrDraw = AddComponent<PNTStaticDraw>();

		//PtrDraw->SetDiffuse(Color4(1.0, 0.0, 0.0, 1.0));	色変え処理

		PtrDraw->SetDiffuse(Color4(1.0, 0.0, 0.0, 1.0));

		//自分の色を見る
		auto MyColor = PtrDraw->GetDiffuse();

		//自分の色が赤（Color4([R]1.0,[G]0.0,[B]0.0,[A]1.0)）になっているか見る

		if (MyColor == Color4(1.0, 0.0, 0.0, 1.0))
		{
			//アクションの登録
			auto PtrAction = AddComponent<Action>();
			PtrAction->AddMoveBy(1.0f, Vector3(-1.0f, 0.0f, 0));
			PtrAction->AddMoveBy(3.0f, Vector3(-3.0f, 0.0f, 0));
			PtrAction->AddMoveBy(5.0f, Vector3(-5.0f, 0.0f, 0));

			//ループする
			PtrAction->SetLooped(true);
			//アクション開始
			PtrAction->Run();
		}
		//影を付ける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_SQUARE");

		PtrDraw->SetMeshResource(L"DEFAULT_SQUARE");

		PtrDraw->SetOwnShadowActive(true);
		PtrDraw->SetTextureResource(L"PICTATTACK_TX");

		auto Group = GetStage()->GetSharedObjectGroup(L"AttackObject");
		Group->IntoGroup(GetThis<GameObject>());

		//透明処理
		SetAlphaActive(true);

	}
}