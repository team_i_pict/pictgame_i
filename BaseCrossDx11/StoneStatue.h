#pragma once
#include "stdafx.h"

namespace basecross
{
	//----------------------------------------------------------------------------
	//���E�Ɉړ�����Α��i�F�j
	//----------------------------------------------------------------------------

	class LateralObject : public GameObject
	{
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
	public:
		//�\�z�Ɣj��
		LateralObject(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position);

		float Timer = 0;

		//������
		virtual ~LateralObject();
		//����
		virtual void OnCreate()override;
	};

	//----------------------------------------------------------------------------
	//�W�����v����Α��i�ΐF�j
	//----------------------------------------------------------------------------

	class JumpObject : public GameObject
	{
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
	public:
		//�\�z�Ɣj��
		JumpObject(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position);

		//������
		virtual ~JumpObject();
		//����
		virtual void OnCreate()override;
	};


	//----------------------------------------------------------------------------
	//�ǂ�j�󂷂�Α��i�ԐF�j
	//----------------------------------------------------------------------------

	class AttackObject : public GameObject
	{
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
	public:
		//�\�z�Ɣj��
		AttackObject(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position);

		//������
		virtual ~AttackObject();
		//����
		virtual void OnCreate()override;
	};
}
