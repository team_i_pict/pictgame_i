#include "stdafx.h"
#include "Project.h"

namespace basecross {
	//--------------------------------------------------------------------------------------
	//	class Tree : public GameObject;
	//	用途: プレイヤーが当たったら色がとれるオブジェクト（木）
	//--------------------------------------------------------------------------------------
	//構築と破棄
	Tree::Tree(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}
	Tree::~Tree() {}

	//初期化
	void Tree::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		//衝突判定
		auto PtrObb = AddComponent<CollisionObb>();
		PtrObb->SetFixed(true);

		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_CUBE");

		//描画処理
		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		PtrDraw->SetDiffuse(Color4(0.0f, 1.0f, 0.0f, 1.0f));

		//画像の透明処理
		PtrDraw->SetOwnShadowActive(true);
		//テクスチャのセット
		PtrDraw->SetTextureResource(L"WHITE_TX");

	}
	void Tree::OnUpdate()
	{
		auto PtrDraw = AddComponent<PNTStaticDraw>();
		auto MyColor = PtrDraw->GetDiffuse();
		timer += 0.05f;
		//白だった時の処理
		/*if (MyColor==Color4(1.0f,1.0f,1.0f,1.0f))
		{
		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		PtrDraw->SetDiffuse(Color4(1.0f - timer, 1.0f, 1.0f - timer, 1.0f));
		}*/


		if (timer >= 10)
		{
			Hitflg = false;
		}

		if (Hitflg == false)
		{
			auto PtrDraw = AddComponent<PNTStaticDraw>();
			PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
			PtrDraw->SetDiffuse(Color4(0.0f, 1.0f, 0.0f, 0.0f));
		}

		if (Hitflg == true)
		{
			auto PtrDraw = AddComponent<PNTStaticDraw>();
			PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
			PtrDraw->SetDiffuse(Color4(1.0f - timer*0.1, 1.0f, 1.0 - timer*0.1f, 1.0f));
		}

		auto PtrTransform = GetComponent<Transform>();
		auto PtrPos = PtrTransform->GetPosition();
		auto PtrScalse = PtrTransform->GetScale();

		//プレイヤーの取得
		auto PtrPlayer = GetStage()->GetSharedGameObject<Player>(L"Player");
		auto PtrPlayerTrnasform = PtrPlayer->GetComponent<Transform>();
		auto PtrPlayerPos = PtrPlayerTrnasform->GetPosition();
		auto PtrPlayerScalse = PtrPlayerTrnasform->GetScale();

		//原点のを移動させる//右Xp//左Xm(Player)
		auto PtrPlayerXp = PtrPlayerPos.x;// +0.3;
		auto PtrPlayerXm = PtrPlayerPos.x;// -0.3;
		auto PtrPlayerYp = PtrPlayerPos.y;// +0.3;
		auto PtrPlayerYm = PtrPlayerPos.y;// -0.3;

										  //右Xp//左Xm
		auto PtrXp = PtrPos.x + (PtrScalse.x / 2);
		auto PtrXm = PtrPos.x - (PtrScalse.x / 2);
		auto PtrYp = PtrPos.y + (PtrScalse.y / 2);
		auto PtrYm = PtrPos.y - (PtrScalse.y / 2);

		if (PtrPlayerXp  > PtrXm - 0.2 && PtrPlayerXm < PtrXp + 0.2  && PtrPlayerYp > PtrYm - 0.2 &&  PtrPlayerYm  < PtrYp + 0.2
			|| PtrPlayerXm  < PtrXm - 0.2 && PtrPlayerXp > PtrXp + 0.2 && PtrPlayerYp > PtrYm - 0.2 &&  PtrPlayerYm< PtrYp + 0.2) {
			Hitflg = true;
			timer = 0;

		}

	}


};