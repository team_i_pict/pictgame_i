#pragma once
#include "stdafx.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	//	class Tree : public GameObject;
	//	用途: プレイヤーが当たったら色がとれるオブジェクト（木）
	//--------------------------------------------------------------------------------------
	class Tree : public GameObject
	{
		shared_ptr< StateMachine<Tree> >  m_StateMachine;

		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
		bool Hitflg = false;
		float timer = 0;
	public:
		//構築と破棄
		Tree(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
		);
		virtual ~Tree();
		//初期化
		virtual void OnCreate() override;

		//アクセサ
		shared_ptr< StateMachine<Tree> > GetStateMachine() const {
			return m_StateMachine;
		}
		void OnUpdate();
		//操作
	};

}